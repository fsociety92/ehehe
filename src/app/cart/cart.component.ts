import { Component, OnInit } from '@angular/core';
import { db } from 'src/utils/firebase';
import { collection, query, where, getDocs, deleteDoc, doc } from "firebase/firestore";
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';

import FirebaseMethods from 'src/utils/firebaseMethods';
import { getAuth } from 'firebase/auth';
import {find} from "rxjs";
import {logMessages} from "@angular-devkit/build-angular/src/builders/browser-esbuild/esbuild";
import {calculateThresholds} from "@angular-devkit/build-angular/src/utils/bundle-calculator";
import {Sneaker} from "../models/sneaker.model";
import {NgModel} from "@angular/forms";

// @ts-ignore
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
})
//let checkoutButton = document.getElementById("checkoutbutton");
export class CartComponent implements  OnInit {
  checkoutCart(): void {
  }

  removeSneakers(): void{
//        this.sneakers.clear()
        this.sneakers.splice(this.sneakers.id, 1)

//        this.sneakers = this.sneakers.id.filter(() => this.sneakers.id === this.sneakers.id);
  }

  sneakers: any = null;
  constructor(private firebaseMethods: FirebaseMethods, private modalService: NgbModal) { }


  
  async ngOnInit(): Promise<void> {
    const auth = getAuth().onAuthStateChanged(async user => {

      console.log(user);
      const q = query(collection(db, "cart"), where("uid", "==", user?.uid));
      const sneakersSnapshot = await getDocs(q)
      const sneakers: {
        id: string;
        productId:string;
        title: any;
        price: any;
        spec: any;
        img: any;
        size:any;
        user_uid: string;
      }[] = [];


      console.log("petuz");
      console.log(sneakersSnapshot)

      sneakersSnapshot.forEach((doc) => {
        const data = doc.data();
        console.log("petuz 2");
        
        console.log(data);

        sneakers.push({
          id: doc.id,
          productId: data['productId'],
          title: data['title'],
          price: data['price'],
          spec: data['spec'],
          img: data['img'],
          size:data['size'],
          user_uid: data['user_uid']
        });
      });

      this.sneakers = sneakers;
      console.log(this.sneakers);

    })
    return this.removeSneakers()


  }
  // async deleteDoc(id:string){
  //   const idx = this.sneakers.findIndex(x => x.id == id)
  //   // const productsSnapshot = await this.firebaseMethods.removeDocument('cart', this.tours.id)
  //   await deleteDoc(doc(db,'sneakers', this.sneakers[idx].id));
  //   console.log(this.sneakers[idx]);
    
  // }
  open(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.sneakers = `Closed with: ${result}`;
    }, (reason) => {
      this.sneakers = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
