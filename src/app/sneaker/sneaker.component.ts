import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import FirebaseMethods from 'src/utils/firebaseMethods';
import { Sneaker } from '../models/sneaker.model';
import { CartComponent } from '../cart/cart.component';
import { getAuth } from 'firebase/auth';
import {catchError} from "rxjs";
import firebase from "firebase/compat";

// const ALERTS: Alert[] = [
//   {
// 		type: 'info',
// 		message: 'This is an info alert',
// 	}
// ];

@Component({
  selector: 'app-product',
  templateUrl: './sneaker.component.html',
  styleUrls: ['./sneaker.component.css'],
})

export class SneakerComponent implements OnInit {


//  goBack(): void {
//    this._location.back();
//  }


  id: string;
  sneaker: Sneaker = new Sneaker();
  sneakers: Sneaker[] = [new Sneaker()];
  size: string = "36";
  // alerts: Alert[];

  constructor(
    private activatedRoute: ActivatedRoute,
    private firebaseMethods: FirebaseMethods
  ) {
    this.id = String(activatedRoute.snapshot.paramMap.get('id'));
  }
  async ngOnInit(): Promise<void> {
    const sneaker: any = await this.firebaseMethods.getDocumentById(
      'sneakers',
      this.id
    );
    this.sneaker = new Sneaker(sneaker.id, sneaker.title, sneaker.price, sneaker.desc, sneaker.img);
  }
  
  async addToCart(id: string) {
    const auth = getAuth().onAuthStateChanged(async user =>{
          const sneaker = { ...this.sneaker, productId:this.sneaker.id, uid: user?.uid, size:this.size};
            await this.firebaseMethods.create('cart', sneaker);
//            alert("Товар добавлен в корзину")
      if (getAuth()){
        alert("Товар добавлен в корзину")
      }
    });
//    return auth()
  }
//  async deleteFromCart(id: string){
//
//  }

//  async addToCard(id: string){
//    if (const auth = getAuth().onAuthStateChanged(async user) => {
//      const sneaker = { ...this.sneaker, productId:this.sneaker.id, uid: user?.uid, size:this.size };
//      await this.firebaseMethods.create('cart', sneaker);
//      alert("Товар добавлен в корзину")
//    })
//  }



  handleChange(event: Event) {
   this.size = (event.target as HTMLInputElement).value;
  }
}
